﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using Сollect_the_declaration.Components.States;

namespace Сollect_the_declaration
{ 
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public static int ScreenWight { get; internal set; }
        public static int ScreenHight { get; internal set; }

        internal static Color color;
        internal static bool ShowGUI;
        internal static Random random;
        public static int backbufferWidth;
        public static int backbufferHeight;
        public static Vector2 scale;
        private State _currentState;
        private State _nextState;
        private float timer;
        private SpriteFont font;
        private Color exitcolor;

        public void ChangeState(State state)
        {
            _nextState = state;
        }
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            timer = 0;
        }
        protected override void Initialize()
        {
            backbufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            backbufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferHeight = 1080;
            graphics.PreferredBackBufferWidth = 1920;
            graphics.HardwareModeSwitch = false;
            graphics.IsFullScreen = true;
            graphics.SynchronizeWithVerticalRetrace = true;
            graphics.ApplyChanges();
            ScreenHight = graphics.PreferredBackBufferHeight;
            ScreenWight = graphics.PreferredBackBufferWidth;
            random = new Random();
            ShowGUI = true;
            color = Color.Black;
            exitcolor = Color.Transparent;
            scale = new Vector2((float)backbufferWidth /
                (float)GraphicsDevice.PresentationParameters.BackBufferWidth,
                (float)backbufferHeight /
                (float)GraphicsDevice.PresentationParameters.BackBufferHeight);
            Console.WriteLine(backbufferWidth);
            _nextState = new IntroState(this, graphics.GraphicsDevice);
            IsMouseVisible = false;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            font = Content.Load<SpriteFont>("Font/File");
            spriteBatch = new SpriteBatch(GraphicsDevice);
            if (_nextState != null)
            {
                _nextState.Load(Content);
            }
        }        

        protected override void UnloadContent()
        {
            if (_currentState != null)
            {
                _currentState.UnLoad();
            }
        }


        protected override void Update(GameTime gameTime)
        {
            if (_nextState != null)
            {
                UnloadContent();
                LoadContent();
                _currentState = _nextState;
                _nextState = null;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                if (timer > 1)
                    Exit();
                timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                exitcolor = new Color(timer * 2, timer * 2, timer * 2, timer / 10000000);
            }
            else
            {
                timer = 0;
                exitcolor = Color.Transparent;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.G))
            {
                ShowGUI = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.F))
            {
                ShowGUI = false;
            }
            _currentState.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(color); //new Color(255,140,150)
            _currentState.Draw(gameTime, spriteBatch);

            spriteBatch.Begin(transformMatrix: Matrix.CreateScale(1.5f));
            spriteBatch.DrawString(font, "Выходим", new Vector2(20, 20), exitcolor);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
