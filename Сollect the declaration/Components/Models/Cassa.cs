﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Сollect_the_declaration.Components.Managers;

namespace Сollect_the_declaration.Components.Models
{
    class Cassa : Sprite
    {
        public Cassa(Texture2D texture) : base(texture)
        {
        }

        public Cassa(Dictionary<string, Animation> _animations) : base(_animations)
        {
        }

        public Cassa(GraphicsDevice graphicsDevice, Texture2D texture) : base(graphicsDevice, texture)
        {
        }

        public Cassa(GraphicsDevice graphicsDevice, Dictionary<string, Animation> _animations) : base(graphicsDevice, _animations)
        {
        }
    }
}
