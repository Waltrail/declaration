﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Сollect_the_declaration.Components.Managers;

namespace Сollect_the_declaration.Components.Models
{
    public class Answer : Sprite
    {
        public bool RightAnswer = false;
        public string Text;
        public SpriteFont font;
        public Color FontColor;

        public Answer(Texture2D texture) : base(texture)
        {
            FontColor = new Color(21, 25, 24);
        }

        public Answer(Dictionary<string, Animation> _animations) : base(_animations)
        {
            FontColor = new Color(21, 25, 24);
        }

        public Answer(GraphicsDevice graphicsDevice, Texture2D texture) : base(graphicsDevice, texture)
        {
            FontColor = new Color(21, 25, 24);
        }

        public Answer(GraphicsDevice graphicsDevice, Dictionary<string, Animation> _animations) : base(graphicsDevice, _animations)
        {
            FontColor = new Color(21, 25, 24);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!IsRemoved)
            {
                if (_texture != null)
                {
                    spriteBatch.Draw(_texture, position, color);
                }
                else if (animationManager != null)
                {
                    animationManager.Draw(spriteBatch);
                }
                else
                {
                    throw new Exception("There's no animations or sprites");
                }
                if (ShowRectangle)
                {
                    if (rectex != null)
                    {
                        spriteBatch.Draw(rectex, new Vector2(position.X, position.Y), color);
                    }
                }               
                spriteBatch.DrawString(font, Text, Position + new Vector2(6, 6), FontColor);
                
            }
        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            if (!color.Equals(Color.White))
            {
                FontColor = Color.White;
            }
            else
            {
                FontColor = new Color(21, 25, 24);
            }
            base.Update(gameTime, sprites);
        }
    }
}
