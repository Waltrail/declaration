﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Сollect_the_declaration.Components.Managers;

namespace Сollect_the_declaration.Components.Models
{
    public class NPC : Player
    {
        private float timer;
        private float Time;
        public NPC(Texture2D texture) : base(texture) {}

        public NPC(Dictionary<string, Animation> _animations) : base(_animations) { Time = -100; Speed = 1.8f; Velocity.Y = Speed; timer = 10; }

        public NPC(GraphicsDevice graphicsDevice, Texture2D texture) : base(graphicsDevice, texture){ Time = -100; Speed = 1.8f; Velocity.Y = Speed; timer = 10; }

        public NPC(GraphicsDevice graphicsDevice, Dictionary<string, Animation> _animations) : base(graphicsDevice, _animations){ Time = -100; Speed = 1.8f; Velocity.Y = Speed; timer = 10; }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (Time < 0)
            {
                Time = Game1.random.Next(1, 40)*0.1f;
            }
            Move();            
            foreach (var sprite in sprites)
            {
                if (sprite == this)
                {
                    continue;
                }
                if (sprite is Player)
                {
                    Velocity = -Velocity;
                    continue;
                }
                if (IsTouchingBottom(sprite) || IsTouchingTop(sprite))
                {
                    Velocity.Y = 0;
                    Time = -100;
                    timer = 10;
                }
                if (IsTouchingLeft(sprite) || IsTouchingRight(sprite))
                {
                    Velocity.X = 0;
                    Time = -100;
                    timer = 10;
                }
            }
            if (position.X <= SceneBox.Left)
            {
                Velocity.X = Speed;
                Time = -100;
            }
            else if(position.X >= SceneBox.Right)
            {
                Velocity.X = -Speed;
                Time = -100;
            }
            if (position.Y <= SceneBox.Top)
            {
                Velocity.Y = Speed;
                Time = -100;
            }
            else if (position.Y >= SceneBox.Bottom)
            {
                Velocity.Y = -Speed;
                Time = -100;
            }
            position += Velocity;          
            SetAnimation();
            if (animations != null)
            {
                animationManager.Update(gameTime);
                animationManager.Position = Position;
            }
        }
        private void Move()
        {            
            if (timer >= Time)
            {
                timer = 0;
                Time = -100;
                int x = Game1.random.Next(1, 5);
                switch (x)
                {
                    case 1:
                        Velocity.X = -Speed;
                        break;
                    case 2:
                        Velocity.Y = Speed;
                        break;
                    case 3:
                        Velocity.Y = -Speed;
                        break;
                    case 4:
                        Velocity.X = Speed;
                        break;
                }
            }
        }
    }
}
