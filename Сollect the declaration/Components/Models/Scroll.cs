﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Сollect_the_declaration.Components.Managers;

namespace Сollect_the_declaration.Components.Models
{
    public class Scroll : Sprite
    {   
        public Scroll(Texture2D texture) : base(texture)
        {
        }

        public Scroll(Dictionary<string, Animation> _animations) : base(_animations)
        {
        }

        public Scroll(GraphicsDevice graphicsDevice, Texture2D texture) : base(graphicsDevice, texture)
        {
        }

        public Scroll(GraphicsDevice graphicsDevice, Dictionary<string, Animation> _animations) : base(graphicsDevice, _animations)
        {
        }        
    }
}
