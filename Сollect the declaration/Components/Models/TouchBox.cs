﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Сollect_the_declaration.Components.Models
{
    public class TouchBox : Sprite
    {
        private MouseState _currentMouse;
        private bool isHover;
        private MouseState _prevMouse;
        public Vector2 Offset;
        public short Charge;
        public override Rectangle Rectangle
        {
            get
            {
                if (_texture != null)
                {
                    return new Rectangle((int)position.X * (int)Offset.X, (int)position.Y * (int)Offset.Y, _texture.Width *(int)Offset.X, _texture.Height * (int)Offset.Y);
                }
                if (animations != null)
                {
                    if (animationManager._animation == null)
                    {
                        return new Rectangle(((int)position.X + BoxFix) * (int)Offset.X, (int)position.Y * (int)Offset.Y, (animations.First().Value.Texture.Width / animations.First().Value.FrameCount - BoxFix) * (int)Offset.X, (animations.First().Value.Texture.Height) * (int)Offset.Y);
                    }
                    else
                    {
                        return new Rectangle(((int)position.X + BoxFix) * (int)Offset.X, (int)position.Y * (int)Offset.Y, (animationManager._animation.Texture.Width / animationManager._animation.FrameCount - BoxFix) * (int)Offset.X, (animationManager._animation.Texture.Height) * (int)Offset.X);
                    }
                }
                else
                {
                    throw new Exception("There's no animations or sprites");
                }
            }
        }
        public void Hover(bool hover)
        {
            isHover = hover;
        }

        public TouchBox(Texture2D texture) : base(texture)
        {
            Offset = new Vector2(0);
        }

        public TouchBox(GraphicsDevice graphicsDevice, Texture2D texture) : base(graphicsDevice, texture)
        {
            Offset = new Vector2(0);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var _color = Color.White;
            if (isHover)
            {
                _color = Color.Green;
            }
            spriteBatch.Draw(_texture, position, _color);
        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            _prevMouse = _currentMouse;
            _currentMouse = Mouse.GetState();
            var MouseRectangle = new Rectangle(_currentMouse.X, _currentMouse.Y, 1, 1);
            isHover = false;
            if (MouseRectangle.Intersects(Rectangle))
            {
                isHover = true;
            }
        }
    }
}
