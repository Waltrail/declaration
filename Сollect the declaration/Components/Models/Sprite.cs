﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using Сollect_the_declaration.Components.Managers;

namespace Сollect_the_declaration.Components.Models
{
    public class Sprite : ICloneable
    {
        protected Texture2D rectex;
        public bool State;
        public Texture2D _texture;
        public AnimationManager animationManager;
        protected Dictionary<string, Animation> animations;
        protected Vector2 position;
        protected Vector2 Velocity;
        public int BoxFix;
        public Color color;
        public bool IsRemoved = false;
        public bool ShowRectangle { get; set; }
        public Curve curve;
        public Vector2 Origin
        {
            get
            {
                return new Vector2(Position.X + Rectangle.Width / 2, Position.Y + Rectangle.Height);
            }
        }
        public virtual Rectangle Rectangle
        {
            get
            {
                if (_texture != null)
                {
                    return new Rectangle((int)position.X, (int)position.Y, _texture.Width, _texture.Height);
                }
                if (animations != null)
                {
                    if (animationManager._animation == null)
                    {
                        return new Rectangle((int)position.X, (int)position.Y, animations.First().Value.Texture.Width / animations.First().Value.FrameCount, animations.First().Value.Texture.Height);
                    }
                    else
                    {
                        return new Rectangle((int)position.X, (int)position.Y, animationManager._animation.Texture.Width / animationManager._animation.FrameCount , animationManager._animation.Texture.Height);
                    }
                }
                else
                {
                    throw new Exception("There's no animations or sprites");
                }
            }
        }
        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                if (animationManager != null)
                {
                    animationManager.Position = position;
                }
                if (animationManager != null)
                {
                    animationManager.Position = position;
                }
            }
        }

        public Sprite(Texture2D texture)
        {
            _texture = texture;
            ShowRectangle = false;
            BoxFix = 0;
            color = Color.White;
        }
        public Sprite(GraphicsDevice graphicsDevice, Texture2D texture) : this(texture)
        {
            SetRectangleTexture(graphicsDevice, texture);
            ShowRectangle = true;
            BoxFix = 0;
            color = Color.White;
        }

        protected void SetRectangleTexture(GraphicsDevice graphicsDevice, Texture2D texture)
        {
            var Colors = new List<Color>();
            for (int y = 0; y < Rectangle.Height; y++)
            {
                for (int x = 0; x < Rectangle.Width; x++)
                {
                    if (x == 0 || y == 0 || x == Rectangle.Width - 1 || y == Rectangle.Height - 1)
                    {
                        Colors.Add(new Color(255, 10, 10, 255));
                    }
                    else
                    {
                        Colors.Add(new Color(0, 0, 0, 0));
                    }
                }
            }
            rectex = new Texture2D(graphicsDevice, texture.Width, texture.Height);
            rectex.SetData<Color>(Colors.ToArray());
        }

        public Sprite(Dictionary<string, Animation> _animations)
        {
            animations = _animations;
            animationManager = new AnimationManager(animations.First().Value);
            BoxFix = 0;
            color = Color.White;
        }
        public Sprite(GraphicsDevice graphicsDevice, Dictionary<string, Animation> _animations)
        {
            SetRectangleTexture(graphicsDevice, Rectangle);
            ShowRectangle = true;
            animations = _animations;
            animationManager = new AnimationManager(animations.First().Value);
            BoxFix = 0;
            color = Color.White;
        }
        public virtual void Update(GameTime gameTime, List<Sprite> sprites)
        {
            if (animations != null)
            {
                SetAnimation();
                animationManager.Update(gameTime);
            }
        }

        protected virtual void SetAnimation()
        {
            animationManager.Play(animations.First().Value);
        }

        public void SetRectangleTexture(GraphicsDevice graphicsDevice, Rectangle rectangle)
        {
            var Colors = new List<Color>();
            for (int y = 0; y < rectangle.Height; y++)
            {
                for (int x = 0; x < rectangle.Width; x++)
                {
                    if (x == 0 || y == 0 || x == rectangle.Width - 1 || y == rectangle.Height - 1)
                    {
                        Colors.Add(new Color(255, 10, 10, 255));
                    }
                    else
                    {
                        Colors.Add(new Color(0, 0, 0, 0));
                    }
                }
            }
            rectex = new Texture2D(graphicsDevice, rectangle.Width, rectangle.Height);
            rectex.SetData<Color>(Colors.ToArray());
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!IsRemoved)
            {
                if (_texture != null)
                {
                    spriteBatch.Draw(_texture, position, color);
                }
                else if (animationManager != null)
                {
                    animationManager.Draw(spriteBatch);
                }
                else
                {
                    throw new Exception("There's no animations or sprites");
                }
                if (ShowRectangle)
                {
                    if (rectex != null)
                    {
                        spriteBatch.Draw(rectex, new Vector2(position.X, position.Y), color);
                    }
                }
            }
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public bool IsTouchingLeft(Sprite sprite)
        {
            return this.Rectangle.Right + Velocity.X > sprite.Rectangle.Left &&
                this.Rectangle.Left < sprite.Rectangle.Left &&
                this.Rectangle.Top < sprite.Rectangle.Bottom &&
                this.Rectangle.Bottom > sprite.Rectangle.Top;
        }
        public bool IsTouchingRight(Sprite sprite)
        {
            return this.Rectangle.Left + Velocity.X < sprite.Rectangle.Right &&
                this.Rectangle.Right > sprite.Rectangle.Right &&
                this.Rectangle.Top < sprite.Rectangle.Bottom &&
                this.Rectangle.Bottom > sprite.Rectangle.Top;
        }
        public bool IsTouchingTop(Sprite sprite)
        {
            return this.Rectangle.Bottom + Velocity.Y > sprite.Rectangle.Top &&
                this.Rectangle.Top < sprite.Rectangle.Top &&
                this.Rectangle.Right > sprite.Rectangle.Left &&
                this.Rectangle.Left < sprite.Rectangle.Right;
        }
        public bool IsTouchingBottom(Sprite sprite)
        {
            return this.Rectangle.Top + Velocity.Y < sprite.Rectangle.Bottom &&
                this.Rectangle.Left < sprite.Rectangle.Right &&
                this.Rectangle.Right > sprite.Rectangle.Left &&
                this.Rectangle.Bottom > sprite.Rectangle.Bottom;
        }        
    }
}