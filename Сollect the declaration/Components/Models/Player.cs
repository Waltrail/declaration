﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Сollect_the_declaration.Components.Controls;
using Сollect_the_declaration.Components.Managers;

namespace Сollect_the_declaration.Components.Models
{
    public class Player : Sprite
    {
        public Input input;
        protected float Speed;
        public Rectangle SceneBox;
        public override Rectangle Rectangle
        {
            get
            {
                if (_texture != null)
                {
                    return new Rectangle((int)position.X, (int)position.Y, _texture.Width, _texture.Height);
                }
                if (animations != null)
                {
                    if (animationManager._animation == null)
                    {
                        return new Rectangle((int)position.X + 4, (int)position.Y + BoxFix, animations.First().Value.Texture.Width / animations.First().Value.FrameCount - 8, animations.First().Value.Texture.Height - BoxFix);
                    }
                    else
                    {
                        return new Rectangle((int)position.X + 4, (int)position.Y + BoxFix, animationManager._animation.Texture.Width / animationManager._animation.FrameCount - 8, animationManager._animation.Texture.Height - BoxFix);
                    }
                }
                else
                {
                    throw new Exception("There's no animations or sprites");
                }
            }
        }
        public Player(Texture2D texture) : base(texture)
        {
            input = new Input();
            Speed = 1.6f;
            SceneBox = new Rectangle();
        }       
        public Player(GraphicsDevice graphicsDevice, Texture2D texture) : base(graphicsDevice, texture)
        {
            input = new Input();
            Speed = 1.6f;
            SceneBox = new Rectangle();
        }
        public Player(Dictionary<string, Animation> _animations) : base(_animations)
        {

            input = new Input();
            Speed = 1.6f;
            SceneBox = new Rectangle();
        }
        public Player(GraphicsDevice graphicsDevice, Dictionary<string, Animation> _animations) : base(_animations)
        {
            SetRectangleTexture(graphicsDevice, Rectangle);
            ShowRectangle = true;
            input = new Input();
            Speed = 1.6f;
            SceneBox = new Rectangle();
        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {   
            Move();           
            foreach (var sprite in sprites)
            {
                if (sprite == this || sprite is Scroll || sprite is Cassa || sprite is NPC)
                {
                    continue;
                }               
                if (IsTouchingBottom(sprite) || IsTouchingTop(sprite)) {
                    Velocity.Y = 0;
                }
                if(IsTouchingLeft(sprite)|| IsTouchingRight(sprite))
                {
                    Velocity.X = 0;
                }
            }
            position += Velocity;
            position.X = MathHelper.Clamp(position.X, SceneBox.Left, SceneBox.Right);
            position.Y = MathHelper.Clamp(position.Y, SceneBox.Top, SceneBox.Bottom);
            SetAnimation();
            if (animations != null)
            {
                animationManager.Update(gameTime);
                animationManager.Position = Position;
            }
        }

        public void SetColor(Color color)
        {
            animationManager.Color = color;
        }

        protected override void SetAnimation()
        {
            if (animations != null)
            {
                if(animationManager._animation == null)
                {
                    animationManager.Play(animations.First().Value);
                }
                if (Velocity.X < 0)
                {
                    animationManager.Flip = false;
                    animationManager.Play(animations[key: "runLeft"]);
                }
                else if (Velocity.X > 0)
                {
                    animationManager.Flip = false;
                    animationManager.Play(animations[key: "runRight"]);
                }
                else if(Velocity.Y > 0)
                {
                    animationManager.Flip = false;
                    animationManager.Play(animations[key: "runDown"]);
                }
                else if (Velocity.Y < 0)
                {
                    animationManager.Flip = false;
                    animationManager.Play(animations[key: "runUp"]);
                }
                if(Velocity == Vector2.Zero)
                {
                    animationManager.Stop();
                }
            }
        }                         
        private void Move()
        {
            if (input.GoLeft)
            {
                Velocity.X = -Speed;                    
            }
            else if (input.GoRight)
            {
                Velocity.X = Speed;                    
            }
            else
            {
                Velocity.X = 0;
            }
            if (input.GoUp)
            {
                Velocity.Y = -Speed;
            }
            else if (input.GoDown)
            {
                Velocity.Y = Speed;
            }
            else
            {
                Velocity.Y = 0;
            }
        }
    }
}
