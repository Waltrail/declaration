﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Сollect_the_declaration.Components.Controls;
using Сollect_the_declaration.Components.Managers;
using Сollect_the_declaration.Components.Models;

namespace Сollect_the_declaration.Components.States
{
    public class TestState : State
    {
        public Player player;
        public List<Sprite> sprites;
        public List<Sprite> untouchesBack;
        public List<Sprite> untouchesFront;
        public List<Answers> answers;
        public Input input;
        public Camera camera;
        private int Score = 0;
        private float red = 1;
        private float green = 0;
        private float blue = 0;
        private float white = 0;
        private float addedwhite = 0.01f;
        private float addedcolor = 0.03f;
        private bool isPaused = false;
        private bool notUpdate = false;
        private float answerTimer = -100;
        private float unpauseTimer = 3;
        private int pick = -100;
        private Rectangle SceneBox;
        private Sprite BGI;
        private Sprite askBox;
        private SpriteFont font;
        private KeyboardState pref;
        private bool answered = false;

        public TestState(Game1 game, GraphicsDevice graphicsDevice) : base(game, graphicsDevice)
        {
            _game = game;
            SceneBox = new Rectangle(new Point(1, 1), new Point(1010, 557));
            camera = new Camera(graphicsDevice.Viewport, SceneBox) { OffsetY = 60, OffsetX = 15};
            _graphicsDevice = graphicsDevice;
            
        }        
        public override void Load(ContentManager content)
        {
            _content = content;
            // GUI
            Texture2D UpArrow = _content.Load<Texture2D>("Sprites/arrowup");
            Texture2D RightArrow = _content.Load<Texture2D>("Sprites/arrowright");
            Texture2D LeftArrow = _content.Load<Texture2D>("Sprites/arrowleft");
            Texture2D DownArrow = _content.Load<Texture2D>("Sprites/arrowdown");
            Texture2D exit = _content.Load<Texture2D>("Sprites/exit");
            Texture2D GUIBG = _content.Load<Texture2D>("Sprites/BGI");

            // NPC          
            var NPCRunUp = new Animation(_content.Load<Texture2D>("Sprites/npc/runUp"), 3) { IsLooping = true };
            var NPCRunDown = new Animation(_content.Load<Texture2D>("Sprites/npc/runDown"), 3) { IsLooping = true };
            var NPCRunLeft = new Animation(_content.Load<Texture2D>("Sprites/npc/runLeft"), 3) { IsLooping = true };
            var NPCRunRight = new Animation(_content.Load<Texture2D>("Sprites/npc/runRight"), 3) { IsLooping = true };
            var AnimationNPC = new Dictionary<string, Animation>()
            {
                { "runUp" ,NPCRunUp },                
                { "runLeft" , NPCRunLeft },
                { "runDown", NPCRunDown },
                { "runRight", NPCRunRight }
            };

            // INPUT
            var offsetV = new Vector2(850, 430);
            TouchBox upArrow = new TouchBox(UpArrow) { Position = new Vector2(offsetV.X + 34, offsetV.Y + 9), Charge = 1 , Offset = new Vector2(2) };
            TouchBox rightArrow = new TouchBox(RightArrow) { Position = new Vector2(offsetV.X + 66, offsetV.Y + 34), Charge = 3, Offset = new Vector2(2) };
            TouchBox leftArrow = new TouchBox(LeftArrow) { Position = new Vector2(offsetV.X + 2, offsetV.Y + 34) ,Charge = 2, Offset = new Vector2(2) };
            TouchBox downArrow = new TouchBox(DownArrow) { Position = new Vector2(offsetV.X + 34, offsetV.Y + 60), Charge = 4, Offset = new Vector2(2) };
            TouchBox Exit = new TouchBox(exit) { Position = new Vector2(20, 10), Charge = 0, Offset = new Vector2(2) };

            List<TouchBox> Touches = new List<TouchBox>()
            {
                upArrow,
                downArrow,
                leftArrow,
                rightArrow,
                Exit
            };

            input = new Input
            {
                Up = Microsoft.Xna.Framework.Input.Keys.Up,
                Right = Microsoft.Xna.Framework.Input.Keys.Right,
                Left = Microsoft.Xna.Framework.Input.Keys.Left,
                Down = Microsoft.Xna.Framework.Input.Keys.Down,
                sprites = Touches,
                game = this._game
            };
            BGI = new Sprite(GUIBG) { Position = offsetV, color = new Color(255,255,255,100)};
            font = _content.Load<SpriteFont>("Font/File");

            var askTex = _content.Load<Texture2D>("Sprites/askbox");
            var answerTex = _content.Load<Texture2D>("Sprites/menu/answerbox");
            askBox = new Sprite(askTex) { Position = new Vector2(67, 67) };

            answers = new List<Answers>()
            {
                new Answers(){ Title = "Работнику начислена заработная плата в размере 48 000 руб., имеет на иждивении 2 несовершеннолетних детей. Рассчитайте НДФЛ, подлежащий удержанию из зарплаты",
                    a = new Answer(answerTex){ Text = "А - 6 058 руб", Position = new Vector2(260,300), font = font },
                    b = new Answer(answerTex){ Text = "Б - 6 240 руб", Position = new Vector2(260,360), font = font },
                    c = new Answer(answerTex){ Text = "С - 5 876 руб", Position = new Vector2(260,420), RightAnswer = true, font = font }
                },
                new Answers(){ Title = "Предприятие реализовало покупателю готовую продукцию на сумму 118 000 руб, в т.ч НДС 18 000 руб. Какую запись должен произвести в учете бухгалтер для учета НДС?",
                    a = new Answer(answerTex){ Text = "А - Д90/3 К68", Position = new Vector2(260,300), RightAnswer = true, font = font },
                    b = new Answer(answerTex){ Text = "Б - Д19 К60", Position = new Vector2(260,360), font = font },
                    c = new Answer(answerTex){ Text = "С - Д91/2 К68", Position = new Vector2(260,420), font = font }
                },
                new Answers(){ Title = "Предприятие розничной торговли закупило товар по ценам поставщика с НДС на сумму 400 тыс. руб., реализовало их за 500 тыс. руб. в том числе НДС. Как найти сумму НДС, начисленную с выручки?",
                    a = new Answer(answerTex){ Text = "А -  500 * 18 / 100", Position = new Vector2(260,300), font = font },
                    b = new Answer(answerTex){ Text = "Б -  500 * 18 / 118", Position = new Vector2(260,360), RightAnswer = true , font = font},
                    c = new Answer(answerTex){ Text = "С - (500 - 400) * 18 / 100", Position = new Vector2(260,420), font = font }
                },
                new Answers(){ Title = "Налоговым кодексом установлены виды налогов...",
                    a = new Answer(answerTex){ Text = "А - Прямые и косвенные", Position = new Vector2(260,300) , font = font},
                    b = new Answer(answerTex){ Text = "Б - Уменьшающие и не уменьшающие налогооблагаемую прибыль в зависимости от источника финансирования", Position = new Vector2(260,360), font = font },
                    c = new Answer(answerTex){ Text = "С - Федеральные, региональные и местные", Position = new Vector2(260,420), RightAnswer = true , font = font}
                },
                new Answers(){ Title = "В конце месяца из зарплатой платы работников удержан НДФЛ. Какая запись будет произведена в учете?",
                    a = new Answer(answerTex){ Text = "А - Д70 К68", Position = new Vector2(260,300), RightAnswer = true, font = font },
                    b = new Answer(answerTex){ Text = "Б - Д68 К70", Position = new Vector2(260,360), font = font },
                    c = new Answer(answerTex){ Text = "С - Д69 К70", Position = new Vector2(260,420), font = font }
                },
                new Answers(){ Title = "На предприятии выплаты за I полугодие составили: заработная плата 96 000 руб., премия 1500 руб., материальная помощь 3000 руб., больничный лист 7400 руб., стимулирующая выплата за участие в\r\nсоревнованиях 4500 руб., оплата стоимости 17 000 руб. Рассчитайте сумму страховых взносов в ПФ, ФСС и ФОМС",
                    a = new Answer(answerTex){ Text = "А - 26 180 руб. - ПФ, 3 541 руб. - ФСС, 6 069 руб. - ФОМС", Position = new Vector2(260,300), RightAnswer = true, font = font},
                    b = new Answer(answerTex){ Text = "Б - 25 150 руб. - ПФ, 4 550 руб. - ФСС, 6 075 руб. - ФОМС", Position = new Vector2(260,360), font = font },
                    c = new Answer(answerTex){ Text = "С - 26 180 руб. - ПФ, 3 541 руб. - ФСС, 5 555 руб. - ФОМС", Position = new Vector2(260,420), font = font }
                },
                new Answers(){ Title = "Основной ставкой НДС является?",
                    a = new Answer(answerTex){ Text = "А - 10%", Position = new Vector2(260,300), font = font },
                    b = new Answer(answerTex){ Text = "Б - 18%", Position = new Vector2(260,360), RightAnswer = true, font = font},
                    c = new Answer(answerTex){ Text = "С - 0%", Position = new Vector2(260,420), font = font }
                },
                new Answers(){ Title = "Как классифицируется налоговое планирование?",
                    a = new Answer(answerTex){ Text = "А - Корпоративное, индивидуальное, семейное, личное", Position = new Vector2(260,300), font = font },
                    b = new Answer(answerTex){ Text = "Б - Корпоративное, индивидуальное", Position = new Vector2(260,360), RightAnswer = true, font = font },
                    c = new Answer(answerTex){ Text = "С - Корпоративное, индивидуальное, семейное", Position = new Vector2(260,420), font = font }
                },
                new Answers(){ Title = "Предприятие приобрело у поставщика материалы на сумму 145 850 руб., в т.ч НДС. Рассчитайте сумму НДС",
                    a = new Answer(answerTex){ Text = "А - 22 248,31 руб.", Position = new Vector2(260,300), RightAnswer = true, font = font },
                    b = new Answer(answerTex){ Text = "Б - 26 253 руб", Position = new Vector2(260,360), font = font },
                    c = new Answer(answerTex){ Text = "С - 24 248,33 руб", Position = new Vector2(260,420), font = font }
                },
                new Answers(){ Title = "Какие бухгалтерские проводки соответствуют операции Начислены страховые взносы на сумму заработной платы работников основного производства?",
                    a = new Answer(answerTex){ Text = "А - Д70 К69", Position = new Vector2(260,300), font = font },
                    b = new Answer(answerTex){ Text = "Б - Д50 К69", Position = new Vector2(260,360), font = font },
                    c = new Answer(answerTex){ Text = "С - Д20 К69", Position = new Vector2(260,420),RightAnswer = true, font = font }
                },
            };

            // Player
            var RunUp = new Animation(_content.Load<Texture2D>("Sprites/Player/runup"), 3) { IsLooping = true };
            var RunDown = new Animation(_content.Load<Texture2D>("Sprites/Player/rundown"), 3) { IsLooping = true };
            var RunLeft = new Animation(_content.Load<Texture2D>("Sprites/Player/runleft"), 3) { IsLooping = true };
            var RunRight = new Animation(_content.Load<Texture2D>("Sprites/Player/runright"), 3) { IsLooping = true };
            var AnimationPlayer = new Dictionary<string, Animation>()
            {
                { "runUp" ,RunUp },
                { "runLeft" , RunLeft },
                { "runDown", RunDown },
                { "runRight", RunRight }
            };
            var ScrollTex = _content.Load<Texture2D>("Sprites/level/Scroll");
            var scroll1 = new Scroll(ScrollTex) { Position = new Vector2(265,200) };
            var scroll2 = new Scroll(ScrollTex) { Position = new Vector2(770,118) };
            var scroll3 = new Scroll(ScrollTex) { Position = new Vector2(510,120) };
            var scroll4 = new Scroll(ScrollTex) { Position = new Vector2(410,50) };

            player = new Player(AnimationPlayer) { Position = new Vector2(445,465), BoxFix = 20, SceneBox = SceneBox };
            player.input = input;

            var backgroundtex = _content.Load<Texture2D>("Sprites/level/backgroundtexture");
            var background = new Sprite(backgroundtex)
            {
                Position = new Vector2(1, 1)
            };

            var npc1 = new NPC(AnimationNPC) { Position = new Vector2(100, 170), SceneBox = SceneBox, BoxFix = 20 };
            var npc2 = new NPC(AnimationNPC) { Position = new Vector2(780, 200), SceneBox = SceneBox, BoxFix = 20 };
            var npc3 = new NPC(AnimationNPC) { Position = new Vector2(790, 90), SceneBox = SceneBox, BoxFix = 20 };
            var npc4 = new NPC(AnimationNPC) { Position = new Vector2(450, 342), SceneBox = SceneBox, BoxFix = 20 };
            var npc5 = new NPC(AnimationNPC) { Position = new Vector2(159, 70), SceneBox = SceneBox, BoxFix = 20 };
            //Walls
            #region Walls_texs
            var ComputerRoomWallTex = _content.Load<Texture2D>("Sprites/level/ComputerRoomWall");
            var ComputerRoomTopWall1Tex = _content.Load<Texture2D>("Sprites/level/ComputerRoomTopWall1");

            var ComputerLineWall2Tex = _content.Load<Texture2D>("Sprites/level/ComputerLineWall2");
            var ComputerLineWall1Tex = _content.Load<Texture2D>("Sprites/level/ComputerLineWall1");            
            var ComputerLineTopWall1Tex = _content.Load<Texture2D>("Sprites/level/ComputerLineTopWall1");
            var ComputerLineTopWall2Tex = _content.Load<Texture2D>("Sprites/level/ComputerLineTopWall2");

            var bigShortTopWall1Tex = _content.Load<Texture2D>("Sprites/level/bigShortTopWall1");
            var bigShortTopWall2Tex = _content.Load<Texture2D>("Sprites/level/bigShortTopWall2");
            var bigShortTopWall3Tex = _content.Load<Texture2D>("Sprites/level/bigShortTopWall3");
            var bigShortWall1Tex = _content.Load<Texture2D>("Sprites/level/bigShortWall1");
            var bigShortWall2Tex = _content.Load<Texture2D>("Sprites/level/bigShortWall2");

            var cassaFrontWallTex = _content.Load<Texture2D>("Sprites/level/cassaFrontWall");
            var cassaWallTex = _content.Load<Texture2D>("Sprites/level/cassaWall");
            var cassaWall1Tex = _content.Load<Texture2D>("Sprites/level/cassaWall1");
            var cassaWall2Tex = _content.Load<Texture2D>("Sprites/level/cassaWall2");

            var elevatorTex = _content.Load<Texture2D>("Sprites/level/elevator");
            var elevatorWall1Tex = _content.Load<Texture2D>("Sprites/level/elevatorWall1");
            var elevatorWall2Tex = _content.Load<Texture2D>("Sprites/level/elevatorWall2");

            var holeBotWallTex = _content.Load<Texture2D>("Sprites/level/holeBotWall");
            var holeMidWallTex = _content.Load<Texture2D>("Sprites/level/holeMidWall");
            var holeTopWall1Tex = _content.Load<Texture2D>("Sprites/level/holeTopWall1");
            var holeTopWall2Tex = _content.Load<Texture2D>("Sprites/level/holeTopWall2");
            var holeTopWall3Tex = _content.Load<Texture2D>("Sprites/level/holeTopWall3");
            var holeWall1Tex = _content.Load<Texture2D>("Sprites/level/holeWall1");
            var holeWall2Tex = _content.Load<Texture2D>("Sprites/level/holeWall2");

            var longBotWallTex = _content.Load<Texture2D>("Sprites/level/longBotWall");
            var longTopWall1Tex = _content.Load<Texture2D>("Sprites/level/longTopWall1");
            var longTopWall2Tex = _content.Load<Texture2D>("Sprites/level/longTopWall2");
            var longWallTex = _content.Load<Texture2D>("Sprites/level/longWall");

            var mainBotWall1Tex = _content.Load<Texture2D>("Sprites/level/mainBotWall1");
            var mainBotWall2Tex = _content.Load<Texture2D>("Sprites/level/mainBotWall2");
            var mainTopWall1Tex = _content.Load<Texture2D>("Sprites/level/mainTopWall1");
            var mainTopWall2Tex = _content.Load<Texture2D>("Sprites/level/mainTopWall2");
            var mainWall1Tex = _content.Load<Texture2D>("Sprites/level/mainWall1");
            var mainWall2Tex = _content.Load<Texture2D>("Sprites/level/mainWall2");
            var mainWall3Tex = _content.Load<Texture2D>("Sprites/level/mainWall3");
            var mainWall4Tex = _content.Load<Texture2D>("Sprites/level/mainWall4");

            var topWayWall1Tex = _content.Load<Texture2D>("Sprites/level/topWayWall1");
            var topWayWall2Tex = _content.Load<Texture2D>("Sprites/level/topWayWall2");
            #endregion

            // Walls Sprites
            #region Walls_Sprites
            var ComputerRoomWall = new Sprite(ComputerRoomWallTex) { Position = new Vector2(56,1) };
            var ComputerRoomTopWall1 = new Sprite(ComputerRoomTopWall1Tex) { Position = new Vector2(72, 1) };

            var ComputerLineWall2 = new Sprite(ComputerLineWall2Tex) { Position = new Vector2(903, 33) };
            var ComputerLineWall1 = new Sprite(ComputerLineWall1Tex) { Position = new Vector2(795, 1) };
            var ComputerLineTopWall1 = new Sprite(ComputerLineTopWall1Tex) { Position = new Vector2(440, 1) };
            var ComputerLineTopWall2 = new Sprite(ComputerLineTopWall2Tex) { Position = new Vector2(795, 33) };

            var bigShortTopWall1 = new Sprite(bigShortTopWall1Tex) { Position = new Vector2(490, 68) };
            var bigShortTopWall2 = new Sprite(bigShortTopWall2Tex) { Position = new Vector2(745, 138) };
            var bigShortTopWall3 = new Sprite(bigShortTopWall3Tex) { Position = new Vector2(903, 139) };
            var bigShortWall1 = new Sprite(bigShortWall1Tex) { Position = new Vector2(745, 68) };
            var bigShortWall2 = new Sprite(bigShortWall2Tex) { Position = new Vector2(953, 139) };

            var cassaFrontWall = new Sprite(cassaFrontWallTex) { Position = new Vector2(354, 296) };
            var cassaWall0 = new Sprite(cassaWallTex) { Position = new Vector2(354, 217) };
            var cassaWall = new Sprite(cassaWallTex) { Position = new Vector2(546, 217) };
            var cassaWall1 = new Sprite(cassaWall1Tex) { Position = new Vector2(370, 217) };
            var cassaWall2 = new Sprite(cassaWall2Tex) { Position = new Vector2(476, 217) };

            var elevator = new Sprite(elevatorTex) { Position = new Vector2(430, 499) };
            var elevatorWall1 = new Sprite(elevatorWall1Tex) { Position = new Vector2(430, 375) };
            var elevatorWall2 = new Sprite(elevatorWall2Tex) { Position = new Vector2(474, 375) };

            var holeBotWall = new Sprite(holeBotWallTex) { Position = new Vector2(1, 295) };
            var holeMidWall = new Sprite(holeMidWallTex) { Position = new Vector2(127, 228) };
            var holeTopWall1 = new Sprite(holeTopWall1Tex) { Position = new Vector2(218, 68) };
            var holeTopWall2 = new Sprite(holeTopWall2Tex) { Position = new Vector2(106, 114) };
            var holeTopWall3 = new Sprite(holeTopWall3Tex) { Position = new Vector2(17, 114) };
            var holeWall1 = new Sprite(holeWall1Tex) { Position = new Vector2(1, 114) };
            var holeWall2 = new Sprite(holeWall2Tex) { Position = new Vector2(202, 68) };

            var longBotWall = new Sprite(longBotWallTex) { Position = new Vector2(617, 333) };
            var longTopWall1 = new Sprite(longTopWall1Tex) { Position = new Vector2(601, 240) };
            var longTopWall2 = new Sprite(longTopWall2Tex) { Position = new Vector2(953, 240) };
            var longWall = new Sprite(longWallTex) { Position = new Vector2(995, 240) };

            var mainBotWall1 = new Sprite(mainBotWall1Tex) { Position = new Vector2(295, 375) };
            var mainBotWall2 = new Sprite(mainBotWall2Tex) { Position = new Vector2(492, 375) };
            var mainTopWall1 = new Sprite(mainTopWall1Tex) { Position = new Vector2(311, 144) };
            var mainTopWall2 = new Sprite(mainTopWall2Tex) { Position = new Vector2(475, 144) };
            var mainWall1 = new Sprite(mainWall1Tex) { Position = new Vector2(295, 295) };
            var mainWall2 = new Sprite(mainWall2Tex) { Position = new Vector2(601, 333) };
            var mainWall3 = new Sprite(mainWall3Tex) { Position = new Vector2(295, 144) };
            var mainWall4 = new Sprite(mainWall4Tex) { Position = new Vector2(601, 144) };

            var topWayWall1 = new Sprite(topWayWall1Tex) { Position = new Vector2(474, 69) };
            var topWayWall2 = new Sprite(topWayWall2Tex) { Position = new Vector2(424, 1) };
            #endregion
            // Decorations
            #region Decorations_texs
            var DeBigShort1Tex = _content.Load<Texture2D>("Sprites/level/DeBigShort1");
            var DeBigShort2Tex = _content.Load<Texture2D>("Sprites/level/DeBigShort2");
            var DeBigShort3Tex = _content.Load<Texture2D>("Sprites/level/DeBigShort3");

            var DeCassa1Tex = _content.Load<Texture2D>("Sprites/level/DeCassa1");
            var DeCassa2Tex = _content.Load<Texture2D>("Sprites/level/DeCassa2");
            var DeCassa89Tex = _content.Load<Texture2D>("Sprites/level/DeCassa89");

            var DeCompLine1Tex = _content.Load<Texture2D>("Sprites/level/DeCompLine1");
            var DeCompLine2Tex = _content.Load<Texture2D>("Sprites/level/DeCompLine2");

            var DeCompRoomTex = _content.Load<Texture2D>("Sprites/level/DeCompRoom");

            var DeHole1Tex = _content.Load<Texture2D>("Sprites/level/DeHole1");
            var DeHole2Tex = _content.Load<Texture2D>("Sprites/level/DeHole2");

            var DeLong1Tex = _content.Load<Texture2D>("Sprites/level/DeLong1");

            var DeMain1Tex = _content.Load<Texture2D>("Sprites/level/DeMain1");
            var DeMain2Tex = _content.Load<Texture2D>("Sprites/level/DeMain2");
            var DeMain3Tex = _content.Load<Texture2D>("Sprites/level/DeMain3");
            #endregion
            // Decoration Sprites
            #region Decoration_Sprites
            var DeBigShort1 = new Sprite(DeBigShort1Tex) { Position = new Vector2(503,87) };
            var DeBigShort2 = new Sprite(DeBigShort2Tex) { Position = new Vector2(757, 166) };
            var DeBigShort3 = new Sprite(DeBigShort3Tex) { Position = new Vector2(931, 167) };

            var DeCassa1 = new Sprite(DeCassa1Tex) { Position = new Vector2(377, 245) };
            var DeCassa2 = new Sprite(DeCassa2Tex) { Position = new Vector2(480, 245) };
            var DeCassa89 = new Cassa(DeCassa89Tex) { Position = new Vector2(436, 284) };

            var DeCompLine1 = new Sprite(DeCompLine1Tex) { Position = new Vector2(450, 20) };
            var DeCompLine2 = new Sprite(DeCompLine2Tex) { Position = new Vector2(797, 56) };

            var DeCompRoom = new Sprite(DeCompRoomTex) { Position = new Vector2(74, 20) };

            var DeHole1 = new Sprite(DeHole1Tex) { Position = new Vector2(28, 142) };
            var DeHole2 = new Sprite(DeHole2Tex) { Position = new Vector2(108, 137) };

            var DeLong1 = new Sprite(DeLong1Tex) { Position = new Vector2(645, 269) };

            var DeMain1 = new Sprite(DeMain1Tex) { Position = new Vector2(405, 324) };
            var DeMain2 = new Sprite(DeMain2Tex) { Position = new Vector2(398, 167) };
            var DeMain3 = new Sprite(DeMain3Tex) { Position = new Vector2(477, 167) };
            #endregion

            sprites = new List<Sprite>()
            {
               ComputerRoomWall,
               ComputerRoomTopWall1,
               ComputerLineWall2,
               ComputerLineWall1,
               ComputerLineTopWall1,
               ComputerLineTopWall2,
               bigShortTopWall1,
               bigShortTopWall2,
               bigShortTopWall3,
               bigShortWall1,
               bigShortWall2,
               cassaFrontWall,
               cassaWall0,               
               cassaWall1,
               cassaWall2,
               cassaWall,
               elevator,
               elevatorWall1,
               elevatorWall2,
               holeBotWall,
               holeMidWall,
               holeTopWall1,
               holeTopWall2,
               holeTopWall3,
               holeWall1,
               holeWall2,
               longBotWall,
               longTopWall1,
               longTopWall2,
               longWall,
               mainBotWall1,
               mainBotWall2,
               mainTopWall1,
               mainTopWall2,
               mainWall1,
               mainWall2,
               mainWall3,
               mainWall4,
               topWayWall1,
               topWayWall2,
               DeBigShort1,
               DeBigShort2,
               DeBigShort3,
               DeCassa1,
               DeCassa2,
               DeCassa89,
               DeCompLine1,
               DeCompLine2,
               DeCompRoom,
               DeHole1,
               DeHole2,
               DeLong1,
               DeMain1,
               DeMain2,
               DeMain3,
               scroll1,
               scroll2,
               scroll3,
               scroll4,
               npc1,
               npc2,
               npc3,
               npc4,
               player
            };

            untouchesBack = new List<Sprite>()
            {
                background
            };
            untouchesFront = new List<Sprite>
            {

            };
            camera.Zoom = 5f;
        }
        public override void Update(GameTime gameTime)
        {
            Game1.color = new Color(red + white, green + white, blue + white);
            if (red >= 1 && blue <= 0 && green <= 1)
            {
                green += addedcolor;
            }
            if (red <= 1 && green >= 1 && blue <= 0)
            {
                red -= addedcolor;
            }
            if (green >= 1 && red <= 0 && blue <= 1)
            {
                blue += addedcolor;
            }
            if (green >= 0 && blue >= 1 && red <= 0)
            {
                green -= addedcolor;
            }

            if (blue >= 1 && green <= 0 && red <= 1)
            {
                red += addedcolor;
            }
            if (blue <= 1.1 && red >= 1 && green <= 0)
            {
                blue -= addedcolor;
            }
            white += addedwhite;
            if (white >= 0.5 || white <= 0)
            {
                addedwhite = -addedwhite;
            }
            input.Update(gameTime);
            camera.Follow(player);
            camera.Update(gameTime);            
            if(answerTimer > 0)
            {
                answerTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            else if(Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                notUpdate = false;
                if (answered)
                {
                    pick = -100;
                    answered = false;
                }
                if (unpauseTimer == 1)
                {
                    isPaused = false;
                }
            }
            else
            {
                notUpdate = false;
                if (answered)
                {
                    pick = -100;
                    answered = false;
                }
                if (unpauseTimer == 1)
                {
                    isPaused = false;
                }
            }
            if (!isPaused)
            {
                if (unpauseTimer < 3 && answerTimer <= 0)
                {
                    unpauseTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                }
                foreach (var sprite in sprites.ToArray())
                {
                    if (sprite is Scroll && (player.IsTouchingLeft(sprite) || player.IsTouchingRight(sprite) || player.IsTouchingTop(sprite)))
                    {
                        sprite.IsRemoved = true;
                        Score++;
                    }
                    sprite.Update(gameTime, sprites);
                    if (sprite is NPC && (player.IsTouchingLeft(sprite) || player.IsTouchingRight(sprite) || player.IsTouchingTop(sprite)) && unpauseTimer > 2)
                    {
                        isPaused = true;
                    }
                    if (Score == 4 && sprite is Cassa && (player.IsTouchingLeft(sprite) || player.IsTouchingRight(sprite) || player.IsTouchingTop(sprite)))
                    {
                        _game.Exit();
                    }
                }
                for (int i = 0; i < sprites.Count; i++)
                {
                    if (sprites[i].IsRemoved)
                    {
                        sprites.RemoveAt(i);
                        i--;
                    }
                }
                foreach (var s in untouchesBack)
                {
                    s.Update(gameTime, untouchesBack);
                }
                foreach (var s in sprites)
                {
                    s.Update(gameTime, sprites);
                }
                foreach (var s in untouchesFront)
                {
                    s.Update(gameTime, untouchesFront);
                }
            }
            else
            {
                if (!notUpdate)
                {
                    askBox.Update(gameTime, sprites);
                    if (pick < 0)
                    {
                        pick = Game1.random.Next(0, answers.Count);
                        foreach(var ask in answers)
                        {
                            ask.a.color = Color.White;
                            ask.b.color = Color.White;
                            ask.c.color = Color.White;                            
                        }
                    }                    
                    else
                    {
                        if (input.CheckRectangleTouch(answers[pick].a.Rectangle, input.touchCollection)
                            || Keyboard.GetState().IsKeyDown(Keys.D1) && pref != Keyboard.GetState()
                            || Keyboard.GetState().IsKeyDown(Keys.A) && pref != Keyboard.GetState())
                        {
                            notUpdate = true;
                            answerTimer = 2;
                            if (answers[pick].a.RightAnswer)
                            {                                
                                unpauseTimer = 1;                                                               
                                answers[pick].a.color = Color.Green;
                            }
                            else
                            {                                
                                answers[pick].a.color = Color.Red;
                            }
                            answered = true;
                            return;
                        }
                        if (input.CheckRectangleTouch(answers[pick].b.Rectangle, input.touchCollection)
                            || Keyboard.GetState().IsKeyDown(Keys.D2) && pref != Keyboard.GetState()
                            || Keyboard.GetState().IsKeyDown(Keys.B) && pref != Keyboard.GetState())
                        {
                            notUpdate = true;
                            answerTimer = 2;
                            if (answers[pick].b.RightAnswer)
                            {
                                unpauseTimer = 1;
                                answers[pick].b.color = Color.Green;                                
                            }
                            else
                            {
                                answers[pick].b.color = Color.Red;                                
                            }
                            answered = true;
                            return;
                        }
                        if (input.CheckRectangleTouch(answers[pick].c.Rectangle, input.touchCollection)
                            || Keyboard.GetState().IsKeyDown(Keys.D3) && pref != Keyboard.GetState()
                            || Keyboard.GetState().IsKeyDown(Keys.C) && pref != Keyboard.GetState())
                        {
                            notUpdate = true;
                            answerTimer = 2;
                            if (answers[pick].c.RightAnswer)
                            {
                                unpauseTimer = 1;
                                answers[pick].c.color = Color.Green;                                
                            }
                            else
                            {
                                answers[pick].c.color = Color.Red;                                
                            }
                            answered = true;
                            return;
                        }
                    }
                    foreach (var ask in answers)
                    {
                        ask.a.Update(gameTime, sprites);
                        ask.b.Update(gameTime, sprites);
                        ask.c.Update(gameTime, sprites);
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.Enter))
                    {
                        isPaused = false;
                        unpauseTimer = 1;
                    }
                    pref = Keyboard.GetState();
                }
                
            }
                       
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            /* WORLD */          

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, camera.Transform);
            foreach (var s in untouchesBack)
            {
                s.Draw(gameTime, spriteBatch);
            }
            foreach (var s in sprites)
            {
                s.Draw(gameTime, spriteBatch);
            }
            foreach (var s in untouchesFront)
            {
                s.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();          

            //GUI
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, Matrix.CreateScale(2,2,0) );
            if (Game1.ShowGUI)
            {
                BGI.Draw(gameTime, spriteBatch);
                input.Draw(gameTime, spriteBatch);
            }
            spriteBatch.DrawString(font, "Счет: "+Score, new Vector2(888, 10), Color.WhiteSmoke);            
            spriteBatch.End();
            spriteBatch.Begin();
            if (isPaused)
            {
                spriteBatch.DrawString(font, "Нажми Enter", new Vector2(435, 250), Color.Gold);
                askBox.Draw(gameTime, spriteBatch);
                if (pick >= 0)
                {
                    spriteBatch.DrawString(font, answers[pick].Title, new Vector2(100, 220), Color.Wheat);
                    answers[pick].a.Draw(gameTime, spriteBatch);
                    answers[pick].b.Draw(gameTime, spriteBatch);
                    answers[pick].c.Draw(gameTime, spriteBatch);
                }
            }
            spriteBatch.End();
        }
        public override void UnLoad()
        {
            _content.Unload();
        }
    }
}
