﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Сollect_the_declaration.Components.Controls;
using Сollect_the_declaration.Components.Managers;
using Сollect_the_declaration.Components.Models;

namespace Сollect_the_declaration.Components.States
{
    class IntroState : State
    {
        private float timer;
        private Sprite logos;
        private Camera camera;
        private float index;
        private TouchCollection touchCollection;
        public IntroState(Game1 game, GraphicsDevice graphicsDevice) : base(game, graphicsDevice)
        {
            timer = 0;
            index = 0.0004f;
            Game1.color = new Color(index*2, index, index*3);
            touchCollection = new TouchCollection();
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, camera.Transform);
            logos.Draw(gameTime, spriteBatch);
            spriteBatch.End();
        }

        public override void Load(ContentManager content)
        {
            _content = content;
            var logosimg = _content.Load<Texture2D>("Sprites/logos");
            var logosani = new Dictionary<string, Animation>()
            {
                {"logos", new Animation(logosimg, 3) { Speed = 4f, IsLooping = false} }
            };
            logos = new Sprite(logosani);
            camera = new Camera(_graphicsDevice.Viewport, new Rectangle()) { CameraPosition = new Vector2(logos.Position.X + (logos.Rectangle.Width / 2),0),Zoom = 4 };            
        }

        public override void Update(GameTime gameTime)
        {
            touchCollection = TouchPanel.GetState();
            Game1.color = new Color(index * 2, index, index * 3);
            logos.Update(gameTime, null);
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            index += 0.002f;
            if(timer > 3.99)
            {
                index = 0.0004f;
                camera.CameraPosition = new Vector2(logos.Position.X + (logos.Rectangle.Width / 2), 0);
                if (logos.animationManager._animation.CurrentFrame == logos.animationManager._animation.FrameCount - 1)
                {
                    _game.ChangeState(new TestState(_game, _graphicsDevice));
                }
                timer = 0;
            }
            if(Keyboard.GetState().IsKeyDown(Keys.Escape) || Keyboard.GetState().IsKeyDown(Keys.Space) || touchCollection.Count > 0)
            {
                index = 0.3589005f;
                Game1.color = new Color(index * 2, index, index * 3);
                _game.ChangeState(new TestState(_game, _graphicsDevice));
            }
            camera.Follow(logos);
            camera.Update(gameTime);
        }
        public override void UnLoad()
        {
            _content.Unload();
        }
    }
}
