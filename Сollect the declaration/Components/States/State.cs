﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Сollect_the_declaration.Components.States
{
    public abstract class State
    {       
        protected GraphicsDevice _graphicsDevice;
        protected Game1 _game;
        protected ContentManager _content;

        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);
        public abstract void Update(GameTime gameTime);
        public abstract void Load(ContentManager content);
        public abstract void UnLoad();
        public State(Game1 game, GraphicsDevice graphicsDevice)
        {
            _game = game; _graphicsDevice = graphicsDevice;
        }

    }
}
