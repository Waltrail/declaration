﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System;

namespace Сollect_the_declaration.Components.Controls
{
    public class Button
    {

        public bool isHover;
        public string text = "";
        public event EventHandler Click;
        public Vector2 padding;

        private Texture2D _texture;
        public TouchCollection touchCollection;
        private SpriteFont _font;

        public Vector2 position { get; set; }
        public Color color { get; set; }
        public Rectangle rectangle
        {
            get
            {
                return new Rectangle((int)position.X, (int)position.Y, _texture.Width, _texture.Height);
            }
        }

        public Button(Texture2D texture,string text,SpriteFont font)
        {
            _texture = texture;
            color = Color.Gray;            
            isHover = false;            
            this.text = text;
            _font = font;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var _color = Color.White;
            if (isHover)
            {
                _color = Color.Yellow;
            }
            spriteBatch.Draw(_texture, rectangle, Color.White);
            spriteBatch.DrawString(_font, text, position + padding, _color);
        }

        public void Hover(bool hover)
        {
            isHover = hover;
        }

        public void Update(GameTime gameTime)
        {
            if (CheckRectangleTouch(rectangle, touchCollection))
            {
                isHover = true;
                Click?.Invoke(this, new EventArgs());
            }
            if (isHover)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Space) || Keyboard.GetState().IsKeyDown(Keys.Enter))
                {
                    Click?.Invoke(this, new EventArgs());
                }
            }
        }

        public bool CheckRectangleTouch(Rectangle target, TouchCollection touchCollection)
        {
            if (touchCollection.Count > 0)
            {
                foreach (var touch in touchCollection)
                {
                    if (target.Contains(touch.Position))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
