﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Сollect_the_declaration.Components.Models;

namespace Сollect_the_declaration.Components.Controls
{
    public class Camera
    {
        public Matrix Transform { get; private set; }
        public Matrix scaleMatrix;
        private Matrix Rotation;
        private Matrix OffsetScale;
        private Matrix OffsetView;
        private Vector2 move;
        private Vector2 centre;
        private Vector2 PrefPosition;
        private Vector2 Pilot;
        public float Velosity;
        public float DefVelosity;
        public float VelosityBoost;
        public float VelosityBound;
        private Rectangle SceneBox;
        private Viewport viewport;
        public Vector2 CameraPosition;
        public float OffsetY;
        public float OffsetX;
        private float zoom = 4.0f;
        private float rotation = 0;
        
        public float Zoom
        {
            get { return zoom; }
            set
            {
                zoom = value;
                if (zoom < 0.1f)
                    zoom = 0.1f;
            }
        }

        public Camera(Viewport newViewport, Rectangle SceneBox)
        {
            OffsetY = 0;
            OffsetX = 0;
            DefVelosity = 0.1f;
            VelosityBoost = 0.005f;
            VelosityBound = 0.25f;
            viewport = newViewport;
            move = new Vector2(Game1.random.Next(-15, 15), Game1.random.Next(-15, 15));
            scaleMatrix = Matrix.CreateScale(Game1.scale.X, Game1.scale.Y, 0);
            Velosity = DefVelosity;
            Pilot = new Vector2();
            this.SceneBox = SceneBox;
            CameraPosition = new Vector2();
            centre = new Vector2();
        }
        public void Follow(Sprite target)
        {
            PrefPosition = centre;
            centre = new Vector2(target.Position.X + (target.Rectangle.Width / 2) + OffsetX, target.Position.Y + (target.Rectangle.Height / 2) - OffsetY);

            Pilot = Vector2.Lerp(CameraPosition, centre, Velosity);

            if (SceneBox.Width != 0 && SceneBox.Height != 0) //+ 16 * Zoom * 4 * Game1.scale.X  9 * Zoom * 4 * Game1.scale.Y
            {
                Pilot.X = MathHelper.Clamp(Pilot.X, SceneBox.Left + 16 * Zoom * 4 * Game1.scale.X, SceneBox.Right - 16 * Zoom * 4 * Game1.scale.X); // 1366 - 240.1 // 1920 - 320
                
                Pilot.Y = MathHelper.Clamp(Pilot.Y, SceneBox.Top + 9 * Zoom * 4 * Game1.scale.Y, SceneBox.Bottom - 9 * Zoom * 4 * Game1.scale.Y);
            }
            Rotation = Matrix.CreateRotationZ(rotation);

            var Position = Matrix.CreateTranslation(new Vector3(-Pilot.X, -Pilot.Y, 0));

            OffsetScale = Matrix.CreateScale(new Vector3(Zoom, Zoom, 0));

            OffsetView = Matrix.CreateTranslation(new Vector3(viewport.Width/2,viewport.Height/2,0));     //Game1.ScreenWight / 2 + move.X, Game1.ScreenHight / 2 + move.Y + 150, 0       
            
            Transform = Position * Rotation * (OffsetScale + scaleMatrix) * OffsetView  ;            

            CameraPosition = Pilot;
        }
        public Matrix GetViewMatrix(Vector2 parallax)
        {           
            return Matrix.CreateTranslation(new Vector3(-CameraPosition * parallax, 0.0f)) *                
                Matrix.CreateTranslation(new Vector3(-viewport.X , -viewport.Y, 0.0f)) *
                Rotation *
                (OffsetScale + scaleMatrix) *
                Matrix.CreateTranslation(new Vector3(viewport.X, -viewport.Y, 0.0f));
        }
        public void Follow(Vector2 target)
        {
            var Position = Matrix.CreateTranslation(-target.X, -target.Y, 0);
            var Offset = Matrix.CreateTranslation(Game1.ScreenWight / 2, Game1.ScreenHight / 2, 0);
            Transform = Position * Offset;
        }
        public void Update(GameTime gameTime)
        {
            if(PrefPosition != centre)
            {
                if(Velosity <= VelosityBound)
                {
                    Velosity += VelosityBoost;
                }
            }
            else
            {
                Velosity = DefVelosity;
            }
        }
    }
}
