﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System.Collections.Generic;
using System.Linq;
using Сollect_the_declaration.Components.Models;

namespace Сollect_the_declaration.Components.Controls
{
    public class Input
    {
        public Keys Up;
        public Keys Down;
        public Keys Left;
        public Keys Right;        
        public bool GoLeft;
        public bool GoRight;
        public bool GoUp;
        public bool GoDown;
        public TouchCollection touchCollection;
        public List<TouchBox> sprites;
        public Game1 game;

        public void Update(GameTime gametime)
        {
            GoLeft = false; GoRight = false; GoUp = false; GoDown = false;
            touchCollection = TouchPanel.GetState();

            // 1 - up, 2 - left, 3 - right, 4 - down
            if (sprites.Any())
            {
                if (Game1.ShowGUI)
                {
                    foreach (var s in sprites)
                    {
                        if (CheckRectangleTouch(s.Rectangle, touchCollection))
                        {
                            s.Hover(true);
                            if(s.Charge == 0)
                            {
                                game.Exit();
                            }
                            if (s.Charge == 1)
                            {
                                GoUp = true;
                            }
                            if (s.Charge == 2)
                            {
                                GoLeft = true;
                            }
                            if (s.Charge == 3)
                            {
                                GoRight = true;
                            }
                            if(s.Charge == 4)
                            {
                                GoDown = true;
                            }
                        }
                        else
                            s.Hover(false); 
                    }
                }
                else
                {
                    if (touchCollection.Count > 0) {
                        Game1.ShowGUI = true;
                    }
                }
            }         
            if (Keyboard.GetState().IsKeyDown(Up))
            {
                GoUp = true;
            }
            if (Keyboard.GetState().IsKeyDown(Down))
            {
                GoDown = true;
            }
            if (Keyboard.GetState().IsKeyDown(Left))
            {
                GoLeft = true;
            }
            if (Keyboard.GetState().IsKeyDown(Right))
            {
                GoRight = true;
            }
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            
            foreach(var s in sprites)
            {
                s.Draw(gameTime, spriteBatch);
            }
            
        }
        public bool CheckRectangleTouch(Rectangle target, TouchCollection touchCollection)
        {
            if (touchCollection.Count > 0)
            {
                foreach (var touch in touchCollection)
                {
                    if (target.Contains(touch.Position))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
