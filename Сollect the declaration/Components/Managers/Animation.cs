﻿using Microsoft.Xna.Framework.Graphics;

namespace Сollect_the_declaration.Components.Managers
{
    public class Animation
    {
        public int CurrentFrame { get; set; }
        public int FrameCount { get; private set; }
        public int FrameHight { get { return Texture.Height; } }
        public int FrameWight { get { return Texture.Width / FrameCount; } }
        public float Speed { get; set; }
        public bool IsLooping { get; set; }
        public Texture2D Texture { get; private set; }

        public Animation(Texture2D texture, int frameCount)
        {
            Texture = texture;
            FrameCount = frameCount;
            IsLooping = true;
            Speed = 0.15f;
        }
    }
}
