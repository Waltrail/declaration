﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Сollect_the_declaration.Components.Managers
{
    public class AnimationManager
    {
        public Animation _animation;
        private float timer;
        public Color Color;
        public bool Flip { get; set; }
        public Vector2 Position { get; set; }
        public AnimationManager(Animation animation)
        {
            animation = _animation;
            Color = Color.White;
        }
        public void Play(Animation animation)
        {
            if (_animation == animation)
            {
                return;
            }
            _animation = animation;
            _animation.CurrentFrame = 0;
            timer = 0f;
        }
        public void Stop()
        {
            timer = 0;
            _animation.CurrentFrame = 1;
        }
        public void Update(GameTime gameTime)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            System.Console.WriteLine(_animation.Speed);
            if (timer > _animation.Speed)
            {
                timer = 0f;
                _animation.CurrentFrame++;
                if (_animation.CurrentFrame >= _animation.FrameCount && _animation.IsLooping)
                {
                    _animation.CurrentFrame = 0;
                }
                if (_animation.CurrentFrame >= _animation.FrameCount && !_animation.IsLooping)
                {
                    _animation.CurrentFrame = _animation.FrameCount - 1;
                }
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {

            
            if (!Flip)
            {
                
                spriteBatch.Draw(_animation.Texture,
                                 Position,
                                 new Rectangle(_animation.CurrentFrame * _animation.FrameWight, 0, _animation.FrameWight, _animation.FrameHight),
                                 Color);
            }
            else
            {
                spriteBatch.Draw(_animation.Texture,
                                 Position,
                                 new Rectangle(_animation.CurrentFrame * _animation.FrameWight, 0, _animation.FrameWight, _animation.FrameHight),
                                 Color, 0, Vector2.Zero, 1, SpriteEffects.FlipHorizontally, 0);
            }
        }
    }
}
